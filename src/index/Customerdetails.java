package index;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import net.proteanit.sql.DbUtils;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.JScrollPane;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class Customerdetails extends JFrame {
	
	Connection con=null;
	PreparedStatement pst=null;
	ResultSet rs=null;
	
	String url= "jdbc:mysql://localhost/carsale";
	String uname = "root";
	String pass = "";

	private JPanel contentPane;
	private JTable table;
	private JTextField txtCustID;
	private JTextField txtCustName;
	private JTextField txtCustAddress;
	private JTextField txtCustTel;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Customerdetails frame = new Customerdetails();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Customerdetails() {
		setTitle("Customer Details");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 995, 687);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(209, 58, 749, 517);
		contentPane.add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		table.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"Id", "Name", "Address", "Contact", "Date"
			}
		));
		
		JButton btnFetch = new JButton("Refresh Details");
		btnFetch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				try {
					
					Class.forName("com.mysql.cj.jdbc.Driver");
					con =DriverManager.getConnection(url, uname, pass);
					String query = "SELECT * FROM `customerreg`";
					pst =con.prepareStatement(query);
					rs=pst.executeQuery();
					
					table.setModel(DbUtils.resultSetToTableModel(rs));
					
					
				
				}catch(Exception ex) {
					System.out.println(ex);
				}
				
				
				
				
				
			}
		});
		btnFetch.setFont(new Font("Lucida Bright", Font.BOLD, 13));
		btnFetch.setBounds(787, 585, 171, 42);
		contentPane.add(btnFetch);
		
		JLabel lbID = new JLabel("ID");
		lbID.setHorizontalAlignment(SwingConstants.CENTER);
		lbID.setFont(new Font("Lucida Bright", Font.BOLD, 13));
		lbID.setBounds(20, 231, 126, 13);
		contentPane.add(lbID);
		
		txtCustID = new JTextField();
		txtCustID.setBounds(20, 247, 126, 33);
		contentPane.add(txtCustID);
		txtCustID.setColumns(10);
		
		JLabel lblName = new JLabel("Name");
		lblName.setHorizontalAlignment(SwingConstants.CENTER);
		lblName.setFont(new Font("Lucida Bright", Font.BOLD, 13));
		lblName.setBounds(20, 290, 126, 13);
		contentPane.add(lblName);
		
		txtCustName = new JTextField();
		txtCustName.setColumns(10);
		txtCustName.setBounds(20, 305, 126, 33);
		contentPane.add(txtCustName);
		
		JLabel lbIAddress = new JLabel("Address");
		lbIAddress.setHorizontalAlignment(SwingConstants.CENTER);
		lbIAddress.setFont(new Font("Lucida Bright", Font.BOLD, 13));
		lbIAddress.setBounds(20, 357, 126, 13);
		contentPane.add(lbIAddress);
		
		txtCustAddress = new JTextField();
		txtCustAddress.setColumns(10);
		txtCustAddress.setBounds(20, 369, 126, 33);
		contentPane.add(txtCustAddress);
		
		JLabel lbITel = new JLabel("Telephone");
		lbITel.setHorizontalAlignment(SwingConstants.CENTER);
		lbITel.setFont(new Font("Lucida Bright", Font.BOLD, 13));
		lbITel.setBounds(20, 412, 126, 24);
		contentPane.add(lbITel);
		
		txtCustTel = new JTextField();
		txtCustTel.setColumns(10);
		txtCustTel.setBounds(20, 432, 126, 33);
		contentPane.add(txtCustTel);
		
		JButton btnUpdate = new JButton("Update");
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				try {
					String query = "UPDATE `customerreg` SET `Name`=?,`Address`=?,`Contact`=? WHERE Id=?";
					Class.forName("com.mysql.cj.jdbc.Driver");
					con =DriverManager.getConnection(url, uname, pass);
					pst =con.prepareStatement(query);
					
					//Data validation
					if(txtCustID.getText().length()!=0) {
						
					if(txtCustName.getText().length()!=0) {
					String queryName = "UPDATE `customerreg` SET `Name`=? WHERE Id=?";
					Class.forName("com.mysql.cj.jdbc.Driver");
					con =DriverManager.getConnection(url, uname, pass);
					pst =con.prepareStatement(queryName);
					pst.setString(1,txtCustName.getText());
					pst.setString(2,txtCustID.getText());
					pst.executeUpdate();
					txtCustName.setText("");
					JOptionPane.showMessageDialog(null,"Name Updated");
					}
					
					if(txtCustAddress.getText().length()!=0) {
					String queryAddress = "UPDATE `customerreg` SET `Address`=? WHERE Id=?";
					Class.forName("com.mysql.cj.jdbc.Driver");
					con =DriverManager.getConnection(url, uname, pass);
					pst =con.prepareStatement(queryAddress);
					pst.setString(2,txtCustID.getText());
					pst.setString(1,txtCustAddress.getText());
					pst.executeUpdate();
					txtCustAddress.setText("");
					JOptionPane.showMessageDialog(null,"Address Updated");
					}
					
					if(txtCustTel.getText().length()!=0) {
					String queryTel = "UPDATE `customerreg` SET `Contact`=? WHERE Id=?";
					Class.forName("com.mysql.cj.jdbc.Driver");
					con =DriverManager.getConnection(url, uname, pass);
					pst =con.prepareStatement(queryTel);
					pst.setString(2,txtCustID.getText());
					pst.setString(1,txtCustTel.getText());
					pst.executeUpdate();
					txtCustTel.setText("");
					JOptionPane.showMessageDialog(null,"Contact Number Updated");
					}
					
					}else
						JOptionPane.showMessageDialog(null,"Enter ID to Update");
					
				   
		            }catch(Exception ex) {
		            	JOptionPane.showMessageDialog(null,"Error Occured!"+ex);
		            
		            }
				
			}
		});
		btnUpdate.setFont(new Font("Lucida Bright", Font.BOLD, 13));
		btnUpdate.setBounds(20, 499, 126, 33);
		contentPane.add(btnUpdate);
		
		JButton btnDelete = new JButton("Delete");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				try {
					String query = "DELETE FROM `customerreg` WHERE ID=?";
					Class.forName("com.mysql.cj.jdbc.Driver");
					con =DriverManager.getConnection(url, uname, pass);
					pst =con.prepareStatement(query);
					
					if(txtCustID.getText().length()!=0) {
						pst.setString(1,txtCustID.getText());
						
					}else {
						JOptionPane.showMessageDialog(null,"Enter the ID to delete");
					}
					
					pst.executeUpdate();						
				
		            JOptionPane.showMessageDialog(null,"Successfully Deleted..");
		            }catch(Exception ex) {
		            	
		            }
			}
		});
		btnDelete.setFont(new Font("Lucida Bright", Font.BOLD, 13));
		btnDelete.setBounds(20, 542, 126, 33);
		contentPane.add(btnDelete);
		
		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Admin newWindow2 = new Admin();
	            newWindow2.setVisible(true);
	            setVisible(false);
				
			}
		});
		btnBack.setFont(new Font("Lucida Bright", Font.BOLD, 13));
		btnBack.setBounds(20, 590, 126, 33);
		contentPane.add(btnBack);
		
		JLabel lblCustomerDetails = new JLabel("Customer Details");
		lblCustomerDetails.setForeground(new Color(160, 82, 45));
		lblCustomerDetails.setHorizontalAlignment(SwingConstants.CENTER);
		lblCustomerDetails.setFont(new Font("Lucida Bright", Font.BOLD, 22));
		lblCustomerDetails.setBounds(209, 10, 749, 42);
		contentPane.add(lblCustomerDetails);
		
		JLabel lblNewLabel = new JLabel("New label");
		ImageIcon img1 = new ImageIcon(this.getClass().getResource("/resources/img6.png"));
		lblNewLabel.setIcon(img1);
		lblNewLabel.setBounds(0, 10, 199, 211);
		contentPane.add(lblNewLabel);
	}
}

